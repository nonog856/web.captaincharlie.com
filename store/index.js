
import { v4 as uuidv4 } from 'uuid';

export const state = () => ({
    alerts: []
  })
  
  export const actions = {
    counterUp({ state, commit }){
      commit('setCounter', state.counter + 1)
    }
  }

  export const mutations = {
    addAlert(state, value) {
      value.id = uuidv4();
      state.alerts.push(value)
    },

    removeAlert(state, id) {
      if (!id) return
      state.alerts = state.alerts.filter(alert => alert.id != id)
    },
  }
